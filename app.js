const express = require('express');
const dotenv = require('dotenv');
const userRouter = require('./routes/users');
const tasksRouter = require('./routes/tasks');
const subtasksRouter = require('./routes/subtasks');
const { sequelize } = require('./models');

const app = express();
dotenv.config();

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.listen(4000, async () => {
    try {
        console.log('Server started on 4000');
        await sequelize.sync({ force: false });
        app.use('/user', userRouter);
        app.use('/tasks', tasksRouter);
        app.use('/subtasks', subtasksRouter);
    } catch(err) {
        console.error(err);
        resizeBy.status(500).json(err);
    }
});
