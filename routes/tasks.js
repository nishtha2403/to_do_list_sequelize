const express = require('express');
const authenticateJWT = require('../auth');
const { User, Tasks, Subtasks }  = require('../models');

const router = express.Router();

router.get('/', authenticateJWT, async (req,res) => {
    try {
        const allTasksList = await Tasks.findAll({ attributes: ['id', 'user_id', 'title', 'description', 'is_complete']});
        return res.json(allTasksList);
    } catch(err) {
        console.log(err);
        return res.status(500).json(err);
    }
});

router.get('/:id', authenticateJWT, async (req,res) => {
    try {
        const user_id = req.params.id;
        const specificUserTasks = await Tasks.findAll({ attributes: ['id', 'user_id', 'title', 'description', 'is_complete'], where: {user_id}});
        return res.json(specificUserTasks);
    } catch(err) {
        console.log(err);
        return res.status(500).json(err);
    }
});

router.post('/newtask', authenticateJWT, async (req,res) => {
    try {
        const { email } = req.user;
        const { id: user_id} = await User.findOne({attribute: ['id'], where: {email: email}});
        const { title, description } = req.body;
        const  newTaskStatus  = await Tasks.create({user_id,title,description});

        res.json({msg: "Task Created!"});
    } catch(err) {
        console.log(err);
        return res.status(500).json(err);
    }
});

router.put('/:taskid', authenticateJWT, async (req,res) => {
    try {
        const { email } = req.user;
        const { id: user_id} = await User.findOne({attribute: ['id'], where: {email: email}});
        const id = req.params.taskid;
        const { title , description , is_complete } = req.body;
        const [ taskUpdateStatus ] = await Tasks.update({ title , description , is_complete },{ where: {id,user_id} });

        if(taskUpdateStatus){
            return res.json({msg: "Task Updated!"});
        } else {
            throw new Error('Unauthorized or Invalid Task');
        }
    } catch(err) {
        console.error(err);

        if(err.message === 'Unauthorized or Invalid Task') {
            return res.status(401).json({msg:'Unauthorized or Invalid Task'});
        }

        return res.status(400).json(err);
    }
});

router.delete('/:taskid', authenticateJWT, async (req,res) => {
    try {
        const { email } = req.user;
        const id = req.params.taskid;

        const { id: user_id} = await User.findOne({attribute: ['id'], where: {email}});
        const taskExists = await Tasks.findOne({where: {id,user_id}});

        if(taskExists) {
            const deleteSubtaskStatus = await Subtasks.destroy({ where: {task_id:id} });    
            const deleteTaskStatus = await Tasks.destroy({ where: {id,user_id} });
            if(deleteTaskStatus) {
                return res.json({msg: "Task Deleted!"});
            }
        } else {
            throw new Error('Unauthorized or Invalid Task');
        }
        
    } catch(err) {
        console.error(err);

        if(err.message === 'Unauthorized or Invalid Task') {
            return res.status(401).json({msg:'Unauthorized or Invalid Task'});
        }

        return res.status(400).json(err);
    }
});

module.exports = router;