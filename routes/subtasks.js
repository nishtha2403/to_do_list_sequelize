const express = require('express');
const authenticateJWT = require('../auth');
const { User, Tasks, Subtasks }  = require('../models');

const router = express.Router();

router.get('/', authenticateJWT, async (req,res) => {
    try {
        const subtasksList = await Subtasks.findAll({ attributes: ['id', 'task_id', 'title', 'description', 'is_complete']});
        return res.json(subtasksList);
    } catch(err) {
        console.log(err);
        return res.status(500).json(err);
    }
});

router.get('/:taskid', authenticateJWT, async (req,res) => {
    try {
        const task_id = req.params.taskid;
        const taskIdSubtasks = await Subtasks.findAll({ attributes: ['id', 'task_id', 'title', 'description', 'is_complete'], where: {task_id}});
        if(taskIdSubtasks.length === 0) {
            throw new Error('Invalid Task ID')
        }
        return res.json(taskIdSubtasks);
    } catch(err) {
        console.log(err.message);

        if(err.message === 'Invalid Task ID') {
            return res.status(400).json({msg: 'Invalid Task ID'});
        }
        return res.status(500).json(err);
    }
});

router.post('/newsubtask/:taskid', authenticateJWT, async (req,res) => {
    try {
        const { email } = req.user;
        const id = req.params.taskid;
        const { title, description } = req.body;

        const { id: user_id} = await User.findOne({attribute: ['id'], where: {email: email}});
        const result = await Tasks.findOne({where: {id,user_id}});
        
        if(result){
            const subtask = await Subtasks.create({task_id:id,title,description});
            return res.json(subtask);
        } else {
            return res.status(400).json({msg:'Unauthorized or Invalid Task ID'});
        }
    } catch(err) {
        console.log(err);
        return res.status(500).json(err);
    }
});

router.put('/:subtaskid', authenticateJWT, async (req,res) => {
    try {
        const { email } = req.user;
        const id = req.params.subtaskid;
        const {title, description, is_complete} = req.body;

        const { id: user_id} = await User.findOne({attribute: ['id'], where: {email: email}});
        const { task_id } = await Subtasks.findOne({where: {id}});
        const result = await Tasks.findOne({where: {id:task_id,user_id}});

        if(result) {
            const updateSubtask = await Subtasks.update({title, description, is_complete},{ where: {id,task_id} });
            return res.json(updateSubtask);
        } else {
            throw new Error('Unauthorized');
        }
    } catch(err) {
        console.error(err);

        if(err.message === 'Unauthorized') {
            return res.status(401).json({msg:'Unauthorized'});
        }
        if(err.message === `Cannot destructure property 'task_id' of '(intermediate value)' as it is null.`) {
            return res.status(400).json({msg: 'Invalid Subtask ID'});
        }

        return res.status(400).json(err);
    }
});

router.delete('/:subtaskid', authenticateJWT, async (req,res) => {
    try {
        const { email } = req.user;
        const id = req.params.subtaskid;

        const { id: user_id} = await User.findOne({attribute: ['id'], where: {email: email}});
        const { task_id } = await Subtasks.findOne({ where: {id}});
        const result = await Tasks.findOne({where: {id:task_id,user_id}});

        if(result || !task_id) {
            const deleteSubtask = await Subtasks.destroy({ where: {id,task_id} });
            return res.json(deleteSubtask);
        } else {
            throw new Error('Unauthorized');
        }
        
    } catch(err) {
        console.error(err);

        if(err.message === 'Unauthorized') {
            return res.status(401).json({msg:'Unauthorized'});
        }
        if(err.message === `Cannot destructure property 'task_id' of '(intermediate value)' as it is null.`) {
            return res.status(400).json({msg: 'Invalid Subtask ID'});
        }

        return res.status(400).json(err);
    }
});

module.exports = router;