'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Subtasks extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Tasks }) {
      // define association here
      this.belongsTo(Tasks, {foreignKey: 'task_id'});
    }
  };
  Subtasks.init({
    title: {
      type:DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {msg: 'Title must be there'},
        notEmpty: {msg: 'Title cannot be empty'}
      }
    },
    description: {
      type:DataTypes.TEXT,
    },
    is_complete: {
      type:DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {
    sequelize,
    tableName: 'subtasks',
    modelName: 'Subtasks',
  });
  return Subtasks;
};